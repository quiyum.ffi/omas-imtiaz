<?php
/**
 * Created by PhpStorm.
 * User: kalponik
 * Date: 8/5/2017
 * Time: 8:54 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Super_admins extends Database
{
    public $id;
    public $admin_name;
    public $email;
    public $admin_contact;
    public $password;
    public $picture;
    public $admin_id;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('name', $data)) {
            $this->admin_name = $data['name'];
        }
        if (array_key_exists('contact', $data)) {
            $this->admin_contact = $data['contact'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('degree', $data)) {
            $this->degree = $data['degree'];
        }
        if (array_key_exists('picture_name', $data)) {
            $this->picture = $data['picture_name'];
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }
        if (array_key_exists('pass', $data)) {
            $this->password = md5($data['pass']);
        }

        return $this;

    }

    public function is_exist_email(){

        $query="SELECT * FROM `super_admin` WHERE `admin_email`='$this->email'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function adminId(){
        $sql = "SELECT id FROM `super_admin` WHERE `admin_email`='$this->email' AND `admin_password`='$this->password'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function adminLogin(){
        $query = "SELECT * FROM `super_admin` WHERE `admin_email`='$this->email' AND `admin_password`='$this->password'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();


        $count = $STH->rowCount();
        if ($count > 0) {

            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function loginCheck(){
        $query = "SELECT * FROM `super_admin` WHERE `admin_email`='$this->email' AND `admin_password`='$this->password'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();


        $count = $STH->rowCount();
        if ($count > 0) {

            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function showUser(){
        $sql = "SELECT * FROM `super_admin` WHERE `admin_email`='$this->email'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function showProfile(){
        $sql = "SELECT * FROM `super_admin` WHERE id='$this->admin_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function log_out(){
        $_SESSION['email']="";
        return TRUE;
    }
    public function logged_in(){
        if ((array_key_exists('email', $_SESSION)) && (!empty($_SESSION['email']))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }


}