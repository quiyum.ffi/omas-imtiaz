<?php
/**
 * Created by PhpStorm.
 * User: kalponik
 * Date: 8/5/2017
 * Time: 8:57 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Doctor_master extends Database
{
    public $id;
    public $name;
    public $contact;
    public $email;
    public $password;
    public $date;
    public $gender;
    public $degree;
    public $status;
    public $picture;
    public $doctor_id;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('contact', $data)) {
            $this->contact = $data['contact'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('degree', $data)) {
            $this->degree = $data['degree'];
        }
        if (array_key_exists('picture_name', $data)) {
            $this->picture = $data['picture_name'];
        }
        if (array_key_exists('gender', $data)) {
            $this->gender = $data['gender'];
        }
        if (array_key_exists('pass', $data)) {
            $this->password = md5($data['pass']);
        }
        if (array_key_exists('doctor_id', $data)) {
            $this->doctor_id = $data['doctor_id'];
        }

        return $this;

    }

    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO `doctor_master`(doctor_name,contact,gender,degree,password,email,picture,reg_date) VALUES (?,?,?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->name);
        $STH->bindParam(2,$this->contact);
        $STH->bindParam(3,$this->gender);
        $STH->bindParam(4,$this->degree);
        $STH->bindParam(5,$this->password);
        $STH->bindParam(6,$this->email);
        $STH->bindParam(7,$this->picture);
        $STH->bindParam(8,$this->date);

         $STH->execute();

    }
    public function is_exist_email(){

        $query="SELECT * FROM `doctor_master` WHERE `email`='$this->email'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function doctorLogin(){
        $query = "SELECT * FROM `doctor_master` WHERE `email`='$this->email' AND `password`='$this->password'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();


        $count = $STH->rowCount();
        if ($count > 0) {

            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function loginCheck(){
        $query = "SELECT * FROM `doctor_master` WHERE `user_name`='$this->email' AND `password`='$this->password'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();


        $count = $STH->rowCount();
        if ($count > 0) {

            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function doctorId(){
        $sql = "SELECT id,status FROM `doctor_master` WHERE `email`='$this->email' AND `password`='$this->password'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function showUser(){
        $sql = "SELECT * FROM `doctor_master` WHERE `email`='$this->email'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function log_out(){
        $_SESSION['email']="";
        return TRUE;
    }
    public function logged_in(){
        if ((array_key_exists('email', $_SESSION)) && (!empty($_SESSION['email']))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function showall(){
        $sql = "SELECT doctor_master.*,hospital_master.hospital_name FROM `doctor_master`,hospital_master,doctor_details WHERE doctor_master.`status`='1' AND doctor_master.id=doctor_details.doctor_master_id AND doctor_details.hospital_id=hospital_master.id GROUP BY doctor_details.doctor_master_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function showOne(){
        $sql = "SELECT doctor_master.*,doctor_details.*,hospital_master.hospital_name,category.category_name FROM `doctor_master`,hospital_master,doctor_details,category WHERE doctor_master.id=doctor_details.doctor_master_id AND doctor_details.category_id=category.id AND doctor_details.hospital_id=hospital_master.id AND doctor_master.id='$this->id' GROUP BY doctor_details.doctor_master_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function showProfile(){
        $sql = "SELECT doctor_master.*,doctor_details.*,hospital_master.hospital_name,category.category_name FROM `doctor_master`,hospital_master,doctor_details,category WHERE doctor_master.id=doctor_details.doctor_master_id AND doctor_details.category_id=category.id AND doctor_details.hospital_id=hospital_master.id AND doctor_master.id='$this->doctor_id' GROUP BY doctor_details.doctor_master_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }


    public function blockDoctor(){
        $this->status='0';
        $query= "UPDATE doctor_master SET status =? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->status);
        $STH->execute();
    }

    public function deactiveDoctor(){
        $this->status='2';
        $query= "UPDATE doctor_master SET status =? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->status);
        $STH->execute();
    }

    public function activeDoctor(){
        $this->status='1';
        $query= "UPDATE doctor_master SET status =? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->status);
        $STH->execute();
    }
    public function reactiveDoctor(){
        $this->status='1';
        $query= "UPDATE doctor_master SET status =? WHERE `email`='$this->email' AND `password`='$this->password'";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->status);
        $STH->execute();
    }
    public function showallBlocked(){
        $sql = "SELECT doctor_master.*,hospital_master.hospital_name FROM `doctor_master`,hospital_master,doctor_details WHERE doctor_master.`status`='0' AND doctor_master.id=doctor_details.doctor_master_id AND doctor_details.hospital_id=hospital_master.id GROUP BY doctor_details.doctor_master_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }

}