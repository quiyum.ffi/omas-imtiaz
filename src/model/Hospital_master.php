<?php


namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Hospital_master extends Database
{
    public $id;
    public $hospital_name;
    public $location;
    public $contact;
    public $status;
    public $hospital_id;


    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('hos_name', $data)) {
            $this->hospital_name = $data['hos_name'];
        }
        if (array_key_exists('location', $data)) {
            $this->location = $data['location'];
        }
        if (array_key_exists('contact', $data)) {
            $this->contact = $data['contact'];
        }
        if (array_key_exists('hospital_id', $data)) {
            $this->hospital_id = $data['hospital_id'];
        }

        return $this;

    }

    public function store(){
        $query= "INSERT INTO `hospital_master` (hospital_name,location,contact) VALUES (?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->hospital_name);
        $STH->bindParam(2,$this->location);
        $STH->bindParam(3,$this->contact);

        $STH->execute();
    }
    public function is_exist_hospital(){

        $query="SELECT * FROM `hospital_master` WHERE hospital_name='$this->hospital_name'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function showall(){
        $sql = "SELECT * FROM `hospital_master` WHERE status='1' ORDER BY id DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showone(){
        $sql = "SELECT * FROM `hospital_master` WHERE status='1' AND id='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function showoneDetails(){
        $sql = "SELECT * FROM `hospital_master` WHERE status='1' AND id='$this->hospital_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function showCategorynDoctor(){
        $sql = "SELECT doctor_master.doctor_name,doctor_master.degree,doctor_master.contact,category.category_name FROM `hospital_details`,doctor_master,doctor_details,category WHERE hospital_details.master_id=doctor_details.hospital_id AND doctor_master.id=doctor_details.doctor_master_id AND doctor_details.category_id=hospital_details.category_id AND category.id=hospital_details.category_id AND hospital_details.`master_id`='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
}