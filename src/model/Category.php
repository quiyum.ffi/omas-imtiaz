<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 8/10/2017
 * Time: 11:51 PM
 */

namespace App\model;

if(!isset($_SESSION) )  session_start();
use App\database\Database;
use PDO;
use App\Message\Message;


class Category extends Database
{
    public $id;
    public $name;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('cat_name', $data)) {
            $this->name = $data['cat_name'];
        }
        return $this;

    }

    public function store(){
        $query= "INSERT INTO `category`(category_name) VALUES (?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->name);

        $result = $STH->execute();
        if($result){

            Message::setMessage("Success! Data Insertion successfully Completed!");
        }
        else{
            Message::setMessage("Failed! data has not be inserted!");
        }
    }
    public function showall(){
        $sql = "SELECT * FROM `category` WHERE status='1' ORDER BY id DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function is_exist_category(){

        $query="SELECT * FROM `category` WHERE category_name='$this->name'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function deleteOne(){
        $query = "DELETE FROM `category` WHERE id='$this->id'";
        $this->DBH->exec($query);
    }
}