<?php
require_once ("../vendor/autoload.php");
use App\model\Hospital_master;
use App\model\Hospital_details;
use App\model\Temp;
use App\Message\Message;
use App\Utility\Utility;
$masterObj=new Hospital_master();
$detailsObj=new Hospital_details();
$masterObj->prepareData($_POST);
$status=$masterObj->is_exist_hospital();
if(!$status){
    $masterObj->store();
    $_POST['category_id']=implode(",",$_POST['cat_id']);
    $detailsObj->prepareData($_POST);
    $detailsObj->store();
    $tempObj=new Temp();
    $tempObj->delete();
    Message::setMessage("Success! Data successfully Inserted!");
    Utility::redirect('../views/admin/hospital_list.php');
}
else{
    $tempObj=new Temp();
    $tempObj->delete();
    Message::setMessage("Reject! This hospital name is already stored!");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
