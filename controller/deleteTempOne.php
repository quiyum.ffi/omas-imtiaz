<?php
require_once ("../vendor/autoload.php");
use App\model\Temp;
use App\Utility\Utility;
use App\Message\Message;
$obj=new Temp();
$obj->prepareData($_GET);
$obj->deleteOne();
Message::setMessage("Delete! Delete specific data!");
return Utility::redirect($_SERVER['HTTP_REFERER']);