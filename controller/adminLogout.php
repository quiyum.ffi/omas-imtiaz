<?php

if(!isset($_SESSION) )session_start();
require_once("../vendor/autoload.php");
use App\model\Super_admins;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Super_admins();
$status= $auth->log_out();

session_destroy();
session_start();

Message::setMessage("Success! You Logout successfully!");
Utility::redirect('../views/login.php');