<?php
require_once("../vendor/autoload.php");
use App\model\Doctor_master;
use App\model\Super_admins;
use App\Utility\Utility;
use App\Message\Message;
if($_POST['panel']=='doctor'){
    $auth=new Doctor_master();
    $doctor= $auth->prepareData($_POST)->doctorLogin();
    $doctorData=$auth->doctorId();
    if($doctor){
        if($doctorData->status=='1'){
            $_SESSION['role_status']=0;
            $_SESSION['email']=$_POST['email'];
            $_SESSION['doctor_id']=$doctorData->id;
            Utility::redirect('../views/doctor/index.php');
        }
        elseif($doctorData->status=='2'){
            Message::setMessage("Please Re-active your account!");
            Utility::redirect('../views/reactive.php');
        }
        else{
            Message::setMessage("Your account has been suspend!");
            Utility::redirect('../views/login.php');
        }

    }
    else{
        Message::setMessage("Email and password doesn't match! Try Again");
        Utility::redirect('../views/login.php');

    }
}
else if($_POST['panel']=='admin'){
    $auth2=new Super_admins();
    $admin= $auth2->prepareData($_POST)->adminLogin();
    $admindata=$auth2->adminId();
    if($admin){
        $_SESSION['role_status']=1;
        $_SESSION['email']=$_POST['email'];
        $_SESSION['admin_id']=$admindata->id;

        Utility::redirect('../views/admin/index.php');
    }
    else{
        Message::setMessage("Email and password doesn't match! Try Again");
        Utility::redirect('../views/login.php');

    }
}

?>