<?php
session_start();
require_once ("../vendor/autoload.php");
require_once ("templateLayout/info.php");
use App\Message\Message;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title?></title>
    <!-- for-meta-tags-->
    <?php require_once ("templateLayout/css.php");?>
</head>

<body>
<!-- //banner1 -->
<div class="main" id="home">
    <!-- banner -->
    <?php require_once ("templateLayout/navigation.php");?>
</div>


<!-- banner1 -->
<div class="banner1 jarallax">
    <div class="container">
    </div>
</div>

<div class="services-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="index.php">Home</a><i>|</i></li>
            <li>Sign In</li>
        </ul>
    </div>
</div>
<!-- //banner1 -->

<div class="banner-bottom" id="about">
    <div class="container">
        <?php
        if(isset($_SESSION) && !empty($_SESSION['message'])) {

            $msg = Message::getMessage();

            echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
        }

        ?>
        <h2 class="w3_heade_tittle_agile">Re-active Account</h2>
        <p class="sub_t_agileits">Please! provide your email & password</p>

        <div class="book-appointment">
            <h4>LogIn</h4>
            <form action="../controller/reactive.php" method="post">
                <div class="row same col-md-8 col-md-offset-2">
                    <div class="gaps  col-md-12">
                        <p>Email</p>
                        <input type="email" name="email" placeholder="" required="" />
                    </div>

                    <div class="gaps  col-md-12">
                        <p>Password</p>
                        <input type="password" name="pass" placeholder="" required="" />
                    </div>
                    <input type="submit" value="Re-active">
                </div>
            </form>
        </div>
    </div>
</div>
<!-- footer -->
<?php require_once ("templateLayout/footer.php");?>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- js -->
<?php require_once ("templateLayout/script.php");?>
</body>
</html>