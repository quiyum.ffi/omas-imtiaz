<?php
$data=$auth->showUser();
?>
<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url?>views/admin/index.php" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>D</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>AD</b>MIN</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo base_url?>resources/admin_photos/<?php echo $data->admin_picture?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo $data->admin_name?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo base_url?>resources/admin_photos/<?php echo $data->admin_picture?>" class="img-circle" alt="User Image">

                            <p>
                                <?php echo $data->admin_name?> - Super Admin
                                <small>Admin since <?php echo date("d-m-Y",strtotime($data->date))?></small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="adminProfile.php" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo base_url?>controller/adminLogout.php" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
            </ul>
        </div>
    </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url?>resources/admin_photos/<?php echo $data->admin_picture?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $data->admin_name?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active ">
                <a href="<?php echo base_url?>views/admin/index.php">
                    <i class="fa fa-smile-o "></i> <span>Profile</span>
                </a>
            </li>
            <li class="treeview">
                <a href="">
                    <i class="fa fa-home"></i><span>Category</span>

                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url?>views/admin/add_category.php"><i class="fa fa-circle-o"></i> Add Category </a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="">
                    <i class="fa fa-home"></i><span>Hospital</span>

                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url?>views/admin/add_hospital.php"><i class="fa fa-circle-o"></i> Add New Hospital </a></li>
                    <li><a href="<?php echo base_url?>views/admin/hospital_list.php"><i class="fa fa-circle-o"></i> Hospital List</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="">
                    <i class="fa fa-user"></i><span>Doctor</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url?>views/admin/doctor_list.php"><i class="fa fa-circle-o"></i> Doctor List</a></li>
                    <li><a href="<?php echo base_url?>views/admin/blocked_doctor.php"><i class="fa fa-circle-o"></i> Blocked Doctor List</a></li>

                </ul>
            </li>
            <li class="treeview">
                <a href="">
                    <i class="fa fa-info-circle "></i><span>Emergency</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url?>views/admin/admin_emergency.php"><i class="fa fa-circle-o"></i> Add </a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
