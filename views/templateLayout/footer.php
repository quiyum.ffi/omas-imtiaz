<div class="footer">
    <div class="container">
        <h4>Subscribe to <span>OMAS</span></h4>
        <form action="#" method="post">
            <input type="email" name="Email" placeholder="Enter Your Email..." required="">
            <input type="submit" value="Send">
        </form>
        <div class="agile_footer_copy">
            <div class="w3agile_footer_grids">
                <div class="col-md-4 w3agile_footer_grid">
                    <h3>About Us</h3>
                    <p>Its a system for all the people who does face problem of taking appointment.
                        So this is for them.</p>
                </div>
                <div class="col-md-4 w3agile_footer_grid">
                    <h3>Contact Info</h3>
                    <ul>
                        <li><i class="fa fa-map-marker" aria-hidden="true"></i><span>Chittagong.</span></li>
                        <li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="#">omas@gmail.com</a></li>
                        <li><i class="fa fa-phone" aria-hidden="true"></i>+(018) 21 542 846</li>
                    </ul>
                </div>
                <div class="col-md-4 w3agile_footer_grid w3agile_footer_grid1">
                    <h3>Navigation</h3>
                    <ul>
                        <li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="<?php echo base_url?>views/about.php">About Us</a></li>
                        <li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="<?php echo base_url?>views/hospital.php">Hospital</a></li>
                        <li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="<?php echo base_url?>views/appointment.php">Appointment</a></li>
                        <li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="<?php echo base_url?>views/contact.php">Contact</a></li>
                        <li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="<?php echo base_url?>views/emergency.php">Emergency</a></li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="w3_agileits_copy_right_social">
            <div class="col-md-6 agileits_w3layouts_copy_right">
                <p>&copy; 2017 OMAS. All rights reserved | Design by: Imtiaj</a></p>
            </div>
            <div class="col-md-6 w3_agile_copy_right">
                <ul class="agileits_social_list">
                    <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="w3_agile_rss"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>