<?php
require_once ("../vendor/autoload.php");
require_once ("templateLayout/info.php");
date_default_timezone_set('Asia/Dhaka');
$current_date=date("Y-m-d");
$last_date=date( "Y-m-d", strtotime( "$current_date +2 day" ) );
use App\model\Hospital_details;
use App\model\Hospital_master;
use App\model\Doctor_details;
use App\model\Patients;
$patient=new Patients();
$detailsObj=new Hospital_details();
$hospitalObj=new Hospital_master();
$doctorObj=new Doctor_details();
$allHospitalData=$hospitalObj->showall();
if(isset($_POST['hospital_id'])){
    $detailsObj->prepareData($_POST);
    $hospitalObj->prepareData($_POST);
    $oneHospital=$hospitalObj->showoneDetails();
    $allCat=$detailsObj->showCategory();
}
if(isset($_POST['category_id'])){
    $timestamp = strtotime($_POST['a_date']);
    $day = date('D', $timestamp);
    $_POST['appoint_days']=$day;
    $doctorObj->prepareData($_POST);
    $allDataDoctor=$doctorObj->showDoctor();
   
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title?></title>
    <!-- for-meta-tags-->
    <?php require_once ("templateLayout/css.php");?>
</head>

<body>
<div class="main" id="home">
    <!-- banner -->
    <?php require_once ("templateLayout/navigation.php");?>
</div>
<!-- banner -->
<!-- banner1 -->
<div class="banner1 jarallax appointment_banner">
    <div class="container">
    </div>
</div>

<div class="services-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="index.php">Home</a><i>|</i></li>
            <li>Appointment</li>
        </ul>
    </div>
</div>
<!-- //banner1 -->
<!-- icons -->
<div class="banner-bottom" id="about">
    <div class="container">
        <h2 class="w3_heade_tittle_agile">Appointment</h2>
        <p class="sub_t_agileits">By giving informations here anyone can fix an appointment with a doctor. No one need to register for use this service but must need to provide Name and Telephone number & also take the print out of the serial number.</p>

        <div class="book-appointment">
            <h4>Make an appointment

            </h4>
            <?php

            if(isset($_POST['hospital_id'])){
                ?>
                <a href="appointment.php" style="display: block;width: 15%; text-align: center;
    color: #fff;
    margin: 0 auto;
    padding: 14px 30px;
    font-size: 1.04em;
    cursor: pointer;
    border: 2px solid #de0f17;
    outline: none;
    background: #de0f17;
    font-weight: 600;
    transition: 0.5s all ease;
    -webkit-transition: 0.5s all ease;
    -moz-transition: 0.5s all ease;
    -o-transition: 0.5s all ease;
    -ms-transition: 0.5s all ease;
    letter-spacing: 1px;">Previous</a><br>
                <form  method="post" action="appointment.php">
                    <div class="row same">
                        <div class="gaps col-md-6 col-md-offset-3">
                            <table style="color: white">
                                <tbody>

                                <tr>
                                    <td>Your chosen hospital</td>
                                    <td>:   </td>
                                    <td style="color: #13c4c0"><?php echo $oneHospital->hospital_name?></td>
                                </tr>
                                <tr>
                                    <td>Hospital Location</td>
                                    <td>:   </td>
                                    <td style="color: #13c4c0"><?php echo $oneHospital->location?></td>
                                </tr>
                                <tr>
                                    <td>Hospital Contact</td>
                                    <td>:   </td>
                                    <td style="color: #13c4c0"><?php echo $oneHospital->contact?></td>
                                </tr>
                                </tbody>
                            </table>
                            <br>
                            <p>Category</p>
                            <select class="option" name="category_id">
                                <option value="reject">select a category</option>
                                <?php
                                foreach ($allCat as $data){
                                    ?>
                                    <option value="<?php echo $data->category_id?>"><?php echo $data->cat_name?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="gaps col-md-6 col-md-offset-3">
                            <p>Select Date</p>
                            <input type="date" name="a_date" min="<?php echo $current_date?>" max="<?php echo $last_date?>" required="">
                        </div>
                        <input type="hidden" name="hospital_id_new" value="<?php echo $_POST['hospital_id']?>"/>
                    </div>
                    <div class="clearfix"></div>

                    <input type="submit" value="Next Step">

                </form>
            <?php
            }
            else if(isset($_POST['category_id'])){
                ?>
                <div class="row">
                    <form action="appointment.php" method="post">
                        <input type="hidden" name="hospital_id" value="<?php echo $_POST['hospital_id_new']?>"/>
                        <input type="submit" class="btn btn-info" style="background: #6aa1d2; border-color:#6aa1d2; " value="search with another category">
                    </form>
                    <?php
                        if(!$allDataDoctor){
                            ?>
                            <br>
                            <p style="text-align: center;font-size: 14px">There is no doctor data is available. Search another!</p>
                            <?php
                        }
                        foreach ($allDataDoctor as $oneData){
                            ?>
                            <div class="col-md-2">
                                <div class="col-md-10 col-md-offset-1">
                                    <img src="<?php echo base_url?>resources/doctor_photos/<?php echo $oneData->picture?>" class="img-rounded img-responsive">
                                </div>
                                <p style="text-align: center;font-size: 14px">Dr. <?php echo $oneData->doctor_name?></p>
                                <details style="text-align: center">
                                    <summary>Deegree</summary>
                                    <p><?php echo $oneData->degree?></p>
                                </details>
                                <p style="text-align: center;font-size: 12px;color: #c8dc55">+88-<?php echo $oneData->contact?></p>
                                <p style="text-align: center;font-size: 12px;color: #c8dc55">Day: <?php echo $oneData->days?></p>
                                <p style="text-align: center;font-size: 12px;color: #c8dc55">Time: <?php echo $oneData->time?></p>

                                <form action="appointment-step2.php" method="post">
                                    <input type="hidden" name="doctor_master_id" value="<?php echo $oneData->doctor_master_id?>">
                                    <input type="hidden" name="hospital_master_id" value="<?php echo $oneData->hospital_id?>">
                                    <input type="hidden" name="time" value="<?php echo $oneData->time?>">
                                    <input type="hidden" name="category_id" value="<?php echo $oneData->category_id?>">
                                    <input type="hidden" name="limit" value="<?php echo $oneData->patient_limit?>">
                                    <input type="hidden" name="b_date" value="<?php echo $_POST['a_date']?>" >
                                    <input type="hidden" name="appoint_days" value="<?php echo $_POST['appoint_days']?>" >
                                    <input type="submit" value="Take Appointment" class="btn btn-primary" style="margin-top: 0.5em;font-size: 0.62em;">
                                </form>
                            </div>
                    <?php
                        }

                    ?>

                </div>
            <?php
            }
            else{
                ?>
                <form  method="post" action="appointment.php">
                    <div class="row same">
                        <div class="gaps col-md-6 col-md-offset-3">
                            <p>Hospital</p>
                            <select class="option" name="hospital_id">
                                <option value="reject">select a hospital</option>
                                <?php
                                foreach ($allHospitalData as $data){
                                    ?>
                                    <option value="<?php echo $data->id?>"><?php echo $data->hospital_name?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <input type="submit" value="Next Step">
                </form>
                <?php
            }
            ?>

        </div>
    </div>

</div>

<!-- icons -->




<!-- footer -->
<?php require_once ("templateLayout/footer.php");?>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<?php require_once ("templateLayout/script.php");?>
<!-- //here ends scrolling icon -->
</body>
</html>