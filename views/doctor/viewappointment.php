<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/info.php");
use App\Message\Message;
use App\model\Doctor_master;
$auth= new Doctor_master();
use App\Utility\Utility;
if($_SESSION['role_status']==0){
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
date_default_timezone_set('Asia/Dhaka');
$current_date=date("Y-m-d");
$_POST['current_day']=$current_date;
use App\model\Patients;
$obj=new Patients();
$obj->prepareData($_POST);
$obj->prepareData($_SESSION);
$appointData=$obj->showAppoint();
$timestamp = strtotime($current_date);
$day = date('l', $timestamp);

?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/doctor/css.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php require_once ("../templateLayout/doctor/navigation.php");?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Patient
                <small>Appointment List</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Date: <?php echo $current_date?> (<?php echo $day?>)</h3>

                    <div class="box-tools">
                        <div style="width: 150px;" class="input-group input-group-sm">
                            <input type="text" placeholder="Search" class="form-control pull-right" name="table_search">

                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody><tr>
                            <th>Serial</th>
                            <th>Patient Name</th>
                            <th>Age</th>
                            <th>Phone Number</th>
                            <th>Gender</th>
                            <th>Time</th>
                            <th>Hospital</th>
                            <th>Category</th>
                        </tr>
                        
                        <?php
                            foreach ($appointData as $oneData){
                                ?>
                                <tr>
                                    <td><?php echo $oneData->serial?></td>
                                    <td><?php echo $oneData->patient_name?></td>
                                    <td><?php echo $oneData->age?></td>
                                    <td><?php echo $oneData->contact?></td>
                                    <td><?php echo $oneData->gender?></td>
                                    <td><?php echo $oneData->time?></td>
                                    <td><?php echo $oneData->hospital_name?></td>
                                    <td><?php echo $oneData->category_name?></td>
                                </tr>

                        <?php
                            }
                        ?>
                        
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->
    </div>
    <?php require_once ("../templateLayout/doctor/footer.php");?>
</div>
<!-- ./wrapper -->

<?php require_once ("../templateLayout/doctor/script.php");?>
</body>
</html>
