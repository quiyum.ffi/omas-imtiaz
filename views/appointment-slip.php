<?php
session_start();
require_once ("../vendor/autoload.php");
require_once ("../views/templateLayout/info.php");
use App\model\Patients;
use App\Message\Message;
use App\Utility\Utility;
$obj=new Patients();
$obj->prepareData($_REQUEST);
$allData=$obj->showone();
$status=$obj->exist_token();
if($status){
    $booking_date=date("d/m/Y",strtotime("$allData->booking_date"));
    $appoint_date=date("d/m/Y",strtotime("$allData->appoint_date"));
    $timestamp = strtotime($allData->appoint_date);
    $day = date('l', $timestamp);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title?></title>
    <!-- for-meta-tags-->
    <?php require_once ("templateLayout/css.php");?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
    <script type="text/javascript" src="../resources/plugins/jsPDF/jspdf.plugin.table.js"></script>
    <script type="text/javascript" src="../resources/plugins/jsPDF/jspdf.plugin.cell.js"></script>
    <script type="text/javascript" src="../resources/plugins/jsPDF/split_text_to_size.js"></script>
    <script type="text/javascript" src="../resources/plugins/jsPDF/standard_fonts_metrics.js"></script>
    <script type="text/javascript">
        var doc = new jsPDF();
        var specialElementHandlers = {
            '#editor': function (element, renderer) {
                return true;
            }
        };

        $(document).ready(function() {
            $('#btn').click(function () {
                doc.fromHTML($('#content').html(), 30, 30, {
                    'width': 170,
                    'elementHandlers': specialElementHandlers
                });
                doc.save('<?php echo $allData->id."-".$allData->patient_name?>.pdf');
            });
        });
    </script>
</head>

<body>
<div class="main" id="home">
    <!-- banner -->
    <?php require_once ("templateLayout/navigation.php");?>
</div>
<!-- banner -->


<!-- //banner1 -->
<!-- icons -->
<div class="banner-bottom" id="about">
    <div class="container">
        <?php
        if(isset($_SESSION) && !empty($_SESSION['message'])) {

            $msg = Message::getMessage();

            echo "<p class='help-block' style='color: #ff0000;text-align: center'>$msg</p>";
        }

        ?>
            <div class="book-appointment">
                <div id="content">
                <?php
                if($status){
                    ?>
                    <div style="width: 60%; margin: 0 auto">
                        <h1>Online Medical Appointment System</h1>
                        <hr>
                        <h2 style="text-align: center">Appointment Slip</h2><br>
                        <p style="text-align: center">use token no: <b style="color: white"><?php echo $allData->id?></b> to show your appointment confirmation.</p><br>
                        <h3 style="text-align: center">Patient Details</h3>

                        <div class="row">
                            <div class="col-md-3 ">
                                <img src="../resources/patient_photos/<?php echo $allData->picture?>" class="img-responsive">
                            </div>
                        </div>
                        <p>  Name <span style="margin-left: 120px">: <?php echo $allData->patient_name?></span></p>
                        <p>  Contact <span style="margin-left: 99px"></span>: <?php echo $allData->contact?></p>
                        <p> Gender <span style="margin-left: 104px"></span>: <?php echo $allData->gender?></p>
                        <p>  Age <span style="margin-left: 135px"></span>: <?php echo $allData->age?></p>
                        <p style="text-transform: lowercase "> Email <span style="margin-left: 120px"></span>: <?php echo $allData->email?></p>
                        <p> Booking Date <span style="margin-left: 49px"></span>: <?php echo $booking_date?></p>
                        <p> Appointment Date <span style="margin-left: 6px"></span>: <?php echo $appoint_date?></p>
                        <p> Day <span style="margin-left: 134px"></span>: <?php echo $day?></p>
                        <p> Time <span style="margin-left: 125px"></span>: <?php echo $allData->time?></p>
                        <p> Serial <span style="margin-left: 117px"></span>: <?php echo $allData->serial?></p>
                        <hr>
                        <h3 style="text-align: center">Doctor Details</h3>
                        <p>  Name <span style="margin-left: 120px">: <?php echo $allData->doctor_name?></p>
                        <p>  Degree <span style="margin-left: 106px">: <?php echo $allData->degree?></p>
                        <p> Category <span style="margin-left: 89px">: <?php echo $allData->category_name?></p>
                        <p>  Hospital <span style="margin-left: 96px">: <?php echo $allData->hospital_name?></p>
                    </div>
                    <?php
                }
                else{
                    ?>
                    <div style="width: 60%; margin: 0 auto">
                        <h1>Online Medical Appointment System</h1>
                        <hr>
                        <h3 style="text-align: center;color: red">Error! There are no appoint slip we found!</h3>
                        <p class="text-center"><a href="searchSlip.php" class="btn btn-primary">Search Again</a></p>
                    </div>
                    <?php
                }
                ?>


                </div>
                <br>
                <?php
                if($status){
                    ?>
                    <div style="width: 20%; margin: 0 auto">
                        <a href="pdf.php?id=<?php echo $allData->id?>" class="btn btn-primary">Please Save this page or download pdf</a>
                    </div>
                    <?php
                }

                ?>

            </div>
        <div id="editor"></div>

    </div>

</div>

<!-- icons -->




<!-- footer -->
<?php require_once ("templateLayout/footer.php");?>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<?php require_once ("templateLayout/script.php");?>

<!-- //here ends scrolling icon -->
</body>
</html>